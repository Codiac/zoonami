Zoonami
=======

License of Media:
-----------------

Title: G&T RPG soundtrack
Title URL: https://opengameart.org/content/gt-rpg-soundtrack
Author: Juhani Junkala
Author URL: juhanijunkala.com
License(s): CC-BY 4.0 ( https://creativecommons.org/licenses/by/4.0/legalcode ), OGA-BY 3.0 ( http://static.opengameart.org/OGA-BY-3.0.txt )
Original file name: 13 Juhani Junkala - GT - Battle - Crystal World.mp3
Zoonami file name: zoonami_trainer_battle.ogg
Modifications: Removed silence from start and end for looping. Exported as compressed OGG.
	
Title: Outer Space Music Pack
Title URL: https://opengameart.org/content/outer-space-music-pack
Author: Leonardo Paz
License(s): CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
Original file name: 03_racing_through_asteroids_loop.ogg
Zoonami file name: zoonami_wild_battle.ogg
Modifications: Exported as compressed OGG.
	
Title: GameAudio - UI SFX
Title URL: https://freesound.org/people/GameAudio/sounds/220206/
Author: GameAudio
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Zoonami file name: zoonami_select2.ogg
Modifications: Exported as compressed OGG.
    
Title: Level up, power up, Coin get (13 Sounds)
Title URL: https://opengameart.org/content/level-up-power-up-coin-get-13-sounds
Author: wobbleboxx
Author URL: wobbleboxx.com
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Original file name: Rise05.aif
Zoonami file name: zoonami_level_up.ogg
Modifications: Exported as compressed OGG.

Title: Level up, power up, Coin get (13 Sounds)
Title URL: https://opengameart.org/content/level-up-power-up-coin-get-13-sounds
Author: wobbleboxx
Author URL: wobbleboxx.com
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Original file name: FX01.aif
Zoonami file name: zoonami_healer.ogg
Modifications: Exported as compressed OGG.

Title: Coffee Vending Machine
Title URL: https://freesound.org/people/tosha73/sounds/509826/
Author: tosha73
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Zoonami file name: zoonami_vending_machine.ogg
Modifications: Exported as compressed OGG.
	
Title: Sack of Gold
Title URL: https://opengameart.org/content/sack-of-gold
Author: Amarikah
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Original file name: gold_sack.wav
Zoonami file name: zoonami_coins.ogg
Modifications: Exported as compressed OGG.

Title: Creek Swimming
Title URL: https://freesound.org/people/JazzyBay/sounds/435055/
Author: JazzyBay
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Zoonami file names: zoonami_water_footstep.1.ogg, zoonami_water_footstep.2.ogg, and zoonami_water_footstep.3.ogg
Modifcations: Spliced, reduced background noise, and exported as compressed OGG files.

Title: Minetest the Game Character Model
Title URL: https://github.com/minetest/minetest_game/blob/master/mods/player_api
Author(s): 
	Copyright (C) 2011 celeron55, Perttu Ahola <celeron55@gmail.com>
	Copyright (C) 2012 MirceaKitsune
	Copyright (C) 2012 Jordach
	Copyright (C) 2015 kilbith
	Copyright (C) 2016 sofar
	Copyright (C) 2016 xunto
	Copyright (C) 2016 Rogier-5
	Copyright (C) 2017 TeTpaAka
	Copyright (C) 2017 Desour
	Copyright (C) 2018 stujones11
	Copyright (C) 2019 An0n3m0us
License(s): CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )
Original file name: character.b3d
Zoonami file name: zoonami_npc.b3d
Modifications: No modifications were made.

Title: Mollufant
Title URL: https://opengameart.org/content/mollufant
Author: j0j0n4th4n
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Original file name: mollufant.png
Zoonami file names: zoonami_shellephant_front.png and zoonami_shellephant_back.png
Modifications: Monster was modified by isaiah658 and split into two files. Both modified versions are also released under the CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode ) license.

Title: Flute Snake Lite
Title URL: https://opengameart.org/content/flute-snake-lite
Author: j0j0n4th4n
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )
Original file name: flute_snake2.png
Zoonami file names: zoonami_hypnotile_front.png and zoonami_hypnotile_back.png
Modifications: Monster was modified by isaiah658 and split into two files. Both modified versions are also released under the CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode ) license.

All sounds, textures, and 3D models not listed above
Author: isaiah658
License(s): CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

License of Code:
----------------

mobs_api.register_mob function in lua/mob_api.lua is a modified version from Mobs mod by PilzAdam licensed under WTFPL.

----------------

Cliff avoidance in mobs_api.register_mob.on_step in lua/mobs_api.lua is a modified version from Mobs Redo by TenPlus1.

The MIT License (MIT)

Copyright (c) 2016 TenPlus1

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

----------------

mobs_api.monster.spawn_step function in lua/mobs_api.lua is a modified version from Cube Mobs by Xanthus.

The MIT License (MIT)

Copyright (c) 2020 Xanthus

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

----------------

All code not listed above was created by isaiah658 and is licensed under the MIT license.

Copyright 2020 isaiah658

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
